import type { GlobalThemeOverrides } from "naive-ui";
import nordColors from "@/styles/nord.scss?export";

export const nordTheme: GlobalThemeOverrides = {
  common: {
    bodyColor: nordColors.nord0,
    baseColor: nordColors.nord0,
    textColorBase: nordColors.nord6,
    textColor1: nordColors.nord6,
    textColor2: nordColors.nord5,
    textColor3: nordColors.nord4,
    primaryColor: nordColors.nord8,
    primaryColorHover: nordColors.nord9,
    successColor: nordColors.nord14,
    warningColor: nordColors.nord12,
    errorColor: nordColors.nord11,
    infoColor: nordColors.nord9,
  },
  Layout: {
    headerColor: nordColors.nord1,
    footerColor: nordColors.nord1,
    siderColor: nordColors.nord1,
  },
  Image: {
    toolbarColor: nordColors.nord15,
  },
  Card: {
    color: nordColors.nord2,
    colorEmbedded: nordColors.nord2,
    actionColor: nordColors.nord1,
    titleTextColor: nordColors.nord12,
    textColor: nordColors.nord6,
  },
  Timeline: {
    iconColorInfo: nordColors.nord9,
    iconColorSuccess: nordColors.nord14,
    iconColor: nordColors.nord4,
    titleTextColor: nordColors.nord6,
    contentTextColor: nordColors.nord5,
    metaTextColor: nordColors.nord4,
  },
};
