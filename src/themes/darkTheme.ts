import type { GlobalThemeOverrides } from "naive-ui";

export const darkThemeOverrides: GlobalThemeOverrides = {
  common: {
    bodyColor: "#1B202E",
    baseColor: "#23234f",
    primaryColor: "#2CDB7F",
    primaryColorHover: "#2CDB7E",
    successColor: "#B0F252",
    warningColor: "#FFD84A",
    errorColor: "#F98C48",
    infoColor: "#3BA1E3",
  },
  Layout: {
    headerColor: "#435074",
    footerColor: "#435074",
    siderColor: "#435074",
  },
  Card: {
    color: "#435074",
    colorEmbedded: "#435074",
    actionColor: "#32446E",
    titleTextColor: "#3BA1E3",
  },
};
