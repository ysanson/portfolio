import type { GlobalThemeOverrides } from "naive-ui";

export const lightThemeOverrides: GlobalThemeOverrides = {
  common: {
    bodyColor: "#FEFEFF",
    baseColor: "#2CDEA7",
    primaryColor: "#2CDEA7",
    primaryColorHover: "#2CDBA4",
    successColor: "#27D400",
    warningColor: "#E8CC05",
    errorColor: "#FA8805",
    infoColor: "#3F87E8",
  },
  Layout: {
    headerColor: "#F8F9FF",
    footerColor: "#F8F9FF",
    siderColor: "#F8F9FF",
  },
  Card: {
    color: "#F6F7FF",
    colorEmbedded: "#F6F7FF",
    actionColor: "#EDF2FF",
    titleTextColor: "#3F87E8",
  },
};
