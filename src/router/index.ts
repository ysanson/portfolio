import { createRouter, createWebHistory } from "vue-router";
const HomeView = () => import("../views/HomeView.vue");
const ResumeView = () => import("@/views/ResumeView.vue");
const ProjectsView = () => import("@/views/ProjectsView.vue");

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/resume",
      name: "resume",
      component: ResumeView,
    },
    {
      path: "/projects",
      name: "projects",
      component: ProjectsView,
    },
  ],
});

export default router;
