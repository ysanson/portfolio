import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { createI18n, type I18nOptions } from "vue-i18n";
import frFR from "./locales/fr.json";
import enUS from "./locales/en.json";
import jaJP from "./locales/ja.json";
import "./styles/main.scss";

type MessageSchema = typeof frFR;

const app = createApp(App);

app.use(router);

const lang = () => {
  const navLang = navigator.language;
  if (navLang.includes("fr")) {
    return "fr-FR";
  } else if (navLang.includes("en")) {
    return "en-US";
  } else if (navLang.includes("ja")) {
    return "ja-JP";
  } else {
    return "en-US";
  }
};

const vueI18n = createI18n<
  I18nOptions,
  [MessageSchema],
  "fr-FR" | "en-US" | "ja-JP"
>({
  inheritLocale: true,
  legacy: false,
  locale: lang(),
  fallbackLocale: "en-US",
  messages: {
    "fr-FR": frFR,
    "en-US": enUS,
    "ja-JP": jaJP,
  },
});

app.use(vueI18n);

app.mount("#app");
