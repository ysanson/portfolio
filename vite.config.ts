import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
import ViteCSSExportPlugin from "vite-plugin-css-export";
import { NaiveUiResolver } from "unplugin-vue-components/resolvers";
import vueI18n from "@intlify/vite-plugin-vue-i18n";
import { resolve, dirname } from "node:path";

// eslint-disable-next-line @typescript-eslint/no-var-requires
process.env.VUE_APP_VERSION = require("./package.json").version;
// https://vitejs.dev/config/
export default defineConfig({
  // base:
    // process.env.NODE_ENV === "production"
      // ? "/" + process.env.CI_PROJECT_NAME + "/"
      // : "/",
  plugins: [
    vue(),
    Components({
      resolvers: [NaiveUiResolver()],
    }),
    vueI18n({
      // if you want to use Vue I18n Legacy API, you need to set `compositionOnly: false`
      //compositionOnly: false,
      runtimeOnly: false,

      // you need to set i18n resource including paths !
      include: resolve(
        dirname(fileURLToPath(import.meta.url)),
        "./src/locales/**"
      ),
    }),
    ViteCSSExportPlugin(),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
