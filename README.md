# Portfolio

This is the repository for my [portfolio](https://ysanson.gitlab.io/portfolio/).

## Technologies

This site is built with the following technologies:

- [Vue 3](https://vuejs.org) for the Javascript framework.
- [Vue i18n](https://kazupon.github.io/vue-i18n/) for the translation.
- [Vue router](https://router.vuejs.org) for navigation.
- [Naive-ui](https://www.naiveui.com/en-US/os-theme) for the component library.

## Server

This site is powered by [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/), because of its ease of use and, more importantly, because it's free. 🙂

## Installation

Clone this repository, then go inside the folder. Install the dependencies with `npm install`. Then, run the development server with `npm run dev`. Use `npm run dev:host` to open the development server to the local network. Use `npm run lint` to run ESLint and catch code smells.

To compile for production, along with type checks and minifying, use `npm run build`.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
    1) Run `Extensions: Show Built-in Extensions` from VSCode's command palette
    2) Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.
